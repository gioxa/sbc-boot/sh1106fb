---
status: working
---

# SH1106FB

frame buffer driver for linux centos8.1 SH1106 OLED IIC driver

driver at [linux/drivers/video/fbdev/gioxa/sh1106fb.c](linux/drivers/video/fbdev/gioxa/sh1106fb.c)

and documentation at [linux/Documentation/devicetree/bindings/display/sh1106fb.txt](linux/Documentation/devicetree/bindings/display/sh1106fb.txt)

![image.png](./fb_-_1.jpeg)

## Info

2 modules are created
`sh1106fb.ko` & `gioxa_embedded_logo.ko`

- modinfo sh1106fb
```bash
filename:       /lib/modules/5.4.72-200.el8.armv7hl/drivers/video/fbdev/gioxa/sh1106fb.ko
license:        GPL
author:         Danny Goossen<danny@gioxa.com>
description:    GIOXA FB driver for the SH1106 OLED controller
alias:          of:N*T*Cgioxa,sh1106fb-i2cC*
alias:          of:N*T*Cgioxa,sh1106fb-i2c
alias:          i2c:sh1106fb
depends:        fb_sys_fops,gioxa_embedded_logo,sysfillrect,syscopyarea,sysimgblt
name:           sh1106fb
vermagic:       5.4.72-200.el8.armv7hl SMP mod_unload ARMv7 p2v8 
sig_id:         PKCS#7
signer:         CentOS kernel signing key
sig_key:        51:B4:15:68:3F:58:7A:44:28:09:43:E2:F7:EA:E4:0A:75:E6:69:99
sig_hashalgo:   sha256
signature:      16:DA:0A:AA:1C:35:D2:26:E1:BF:15:CE:59:38:3D:25:2C:3F:0B:58:
                19:CA:15:D7:6C:E9:0F:9F:B9:32:65:8E:DA:E2:43:63:A2:19:2B:22:
                DB:F9:8A:63:93:33:36:CF:2F:EA:FB:0F:A6:DD:C9:71:58:A6:91:BC:
                84:3A:85:CF:29:C6:2D:69:71:D5:3E:3F:3F:F1:C1:4B:9D:F4:D4:98:
                1B:77:49:C1:28:76:96:59:57:75:34:0C:65:F8:EC:32:88:A2:C7:93:
                81:41:19:0E:69:FA:67:9B:5B:38:9A:E6:04:A6:15:20:39:B3:62:A7:
                77:99:A1:51:2D:D5:34:A9:10:79:62:48:AA:05:E9:7C:86:30:D0:CF:
                67:68:3C:F3:82:16:8F:BE:B8:0E:72:A8:F1:2E:20:C0:86:A3:57:37:
                6A:63:77:62:EE:5F:F5:7B:96:4F:C9:48:91:AF:D0:59:DB:5C:CD:BC:
                D0:44:D9:87:D0:97:BC:23:D7:00:5B:C4:30:F9:27:CE:5A:89:BD:89:
                71:7E:96:55:79:C3:00:1B:13:29:F7:BA:6A:DF:AB:B0:EC:E6:DA:D9:
                8B:3A:49:00:B7:71:14:94:57:B5:22:AB:A7:25:19:5C:97:00:49:E9:
                BA:3F:EB:4E:B0:FF:C6:13:6F:50:07:FC:08:82:3B:0C:E2:B0:60:A2:
                69:5C:2A:F7:1E:53:0B:63:74:E8:FB:F7:31:FF:74:3D:C2:82:53:6F:
                85:4A:AA:0F:D9:B3:7D:DB:C3:AE:97:64:26:51:86:DB:DD:B5:BD:0A:
                54:45:31:94:FC:93:72:6E:D2:33:A7:28:D1:79:08:85:31:50:F4:A7:
                DB:6F:CC:DC:3D:38:5B:E4:90:29:80:06:F4:48:0F:3A:F7:EA:61:C0:
                92:A0:D4:56:D4:65:F4:AE:5A:21:7B:B7:67:92:D7:17:D8:4B:EF:81:
                60:55:E7:50:F0:2A:19:FA:3E:1C:AF:EB:E6:AC:3B:BD:60:1B:0F:05:
                B6:BE:54:9C
parm:           refreshrate:uint
```
- modinfo gioxa_embedded_logo

```bash
filename:       /lib/modules/5.4.72-200.el8.armv7hl/drivers/video/fbdev/gioxa/gioxa_embedded_logo.ko
license:        GPL
author:         Danny Goossen<danny@gioxa.com>
description:    LOGO for GiOXA SH1106 OLED controller
depends:        
name:           gioxa_embedded_logo
vermagic:       5.4.72-200.el8.armv7hl SMP mod_unload ARMv7 p2v8 
sig_id:         PKCS#7
signer:         CentOS kernel signing key
sig_key:        51:B4:15:68:3F:58:7A:44:28:09:43:E2:F7:EA:E4:0A:75:E6:69:99
sig_hashalgo:   sha256
signature:      3A:D9:C3:83:3D:D7:36:DD:A6:F4:84:B4:14:9E:1B:B6:C2:8C:21:65:
                A6:C7:D7:D8:E6:6D:17:22:F3:BA:1A:A6:BD:8C:DA:A3:FE:E7:73:76:
                BD:78:9D:13:1D:B8:52:B2:CB:8D:59:C2:31:F0:D2:4C:12:53:DC:67:
                45:EA:AE:06:13:26:CC:BB:90:AB:34:DD:F9:98:D1:F6:0D:63:E4:3E:
                C7:20:82:18:70:71:50:13:01:62:E0:AA:7C:5C:8E:D2:73:ED:4C:C6:
                B5:C2:D7:F9:CF:5D:A9:A3:27:19:49:12:46:AB:CB:41:19:2F:94:19:
                F5:9A:54:15:44:84:35:EB:20:88:7A:E4:44:B2:26:B3:58:7C:D0:76:
                19:85:28:CB:EF:BA:F5:61:C0:AF:AB:94:7A:69:E3:9E:E9:32:4F:37:
                D2:1E:9A:C2:46:85:9F:B2:33:A4:A3:AC:22:5B:EA:00:6C:75:8A:50:
                0C:18:7F:55:52:F7:7E:83:1D:A5:F5:03:53:5F:E6:3B:F4:82:BF:A2:
                9F:0E:24:7B:86:CB:92:D8:3B:F6:3F:D7:8D:97:DD:18:FA:27:CD:A4:
                A4:70:CE:E2:2E:C7:3A:87:68:09:F3:DF:F1:31:21:97:B8:03:FD:77:
                B1:A5:E3:5A:11:6F:30:6E:27:EC:CA:67:08:4D:46:91:91:18:D1:3A:
                58:48:D8:6C:DB:D3:17:18:5B:F6:B2:00:95:3A:3A:CF:1D:24:61:DC:
                D8:54:1A:BE:A1:DA:7F:CA:46:C7:93:36:3E:AE:D5:27:23:8E:0E:D0:
                AC:E2:4A:DC:94:0A:F6:22:23:02:FE:E4:AB:F4:D5:C4:B0:FB:92:5C:
                8B:7A:C9:F0:F0:4C:D0:D2:68:F3:76:41:07:B3:6A:72:D8:4F:06:F7:
                5F:BE:34:AD:49:86:A0:2B:4E:F9:D6:30:89:56:C0:66:9F:1B:D9:C1:
                2B:80:3B:D0:3F:9B:0D:0B:62:57:17:44:FD:C6:B5:7F:43:A5:91:A8:
                3E:F9:5F:27
```



## Usuage

### device tree
```c
  compatible = "allwinner,sun6i-a31-i2c";
      reg = <0x1c2ac00 0x400>;
      interrupts = <0x0 0x6 0x4>;
      clocks = <0x3 0x3b>;
      resets = <0x3 0x2e>;
      pinctrl-names = "default";
      pinctrl-0 = <0x1a>;
      status = "ok";
      #address-cells = <0x1>;
      #size-cells = <0x0>;
      clock-frequency = <1000000>;
      sh1106: oled@3c {
              compatible = "gioxa,sh1106fb-i2c";
              reg = <0x3c>;
              gioxa,height = <64>;
              gioxa,width = <128>;
              gioxa,page-offset = <0>; 
              gioxa,col-offset = <2>;
              gioxa,com-invdir;
              gioxa,com-seq; 
              status = "ok";
      };
```

###  /boot/extlinux/extlinux.conf    

```bash
label CentOS Linux (5.4.72-200.el8.armv7hl) 8 (AltArch)
        kernel /vmlinuz-5.4.72-200.el8.armv7hl
        append ro root=UUID=2abd54df-e30b-4daf-8810-00701174aa82 fbcon=map:1 rd.plymouth=0 plymouth.enable=0 loglevel=3 rd.udev.log_level=3 rd.systemd.show_status=auto
        fdtdir /dtb-5.4.72-200.el8.armv7hl/
        initrd /initramfs-5.4.72-200.el8.armv7hl.img
```

use `fbcon=map:1` to prevent take over and login on the display, if remove, one can login on the display




## DEVELOPMENT: compile and install

on tree at `~/rpmbuild/BUILD/kernel-5.4.el8/linux-5.4.72-200.el8.armv7hl` after kernel build with `rpmbuild -ba  ~/rpmbuild/SPECS/kernel.spec`

### BUILD

```bash
cd ~/rpmbuild/BUILD/kernel-5.4.el8/linux-5.4.72-200.el8.armv7hl

# prepare
make modules_prepare
make -j4 SUBDIRS=scripts/mod
make -j4 SUBDIRS=drivers/video/fbdev modules
```


```bash
# and after edit
make -j4 M=drivers/video/fbdev modules
sudo make -j4 INSTALL_MOD_DIR=drivers/video/fbdev/  M=drivers/video/fbdev/gioxa modules_install
```

### patch created as 

```bash
git diff HEAD -r drivers/video/fbdev > ../../../SOURCES/sh1106fb.patch
```
