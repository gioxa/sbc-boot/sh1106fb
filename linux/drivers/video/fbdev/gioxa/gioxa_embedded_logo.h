
#ifndef __GIOXA_EMBEDDED_H
#define __GIOXA_EMBEDDED_H

#define GIOXA_EMBEDDED_WIDTH 128
#define GIOXA_EMBEDDED_HEIGHT 64

extern unsigned char gioxa_embedded_bits[];

#endif /* __GIOXA_EMBEDDED_H */
